set nocompatible              " be iMproved, required
filetype off                  " required

set ts=4
set sw=4
set sts=4
set number
au Bufenter *.\(py\|c\|cpp\|h\|html\) set et
set smartindent
set ruler
set showcmd
set hlsearch
set encoding=utf-8
set fileencodings=utf-8,euc-kr
syntax enable


"=============== etc =============="
nnoremap <C-H> <C-W>h
nnoremap <C-J> <C-W>j
nnoremap <C-K> <C-W>k
nnoremap <C-L> <C-W>l

nnoremap <F12> :!node %<CR>

















